# Hukobu Air Liveries

This repository contain all the various Hukobu Air Liveries.

Hukobu Air is a Virtual Airline operating through the MSFS addon “OnAir Company” software. More information on their [website](https://sites.google.com/view/hukobuair).

## Liveries

Simply paste the folders in your community folder.

You can [download all](https://gitlab.com/draknova/hukobu-air/-/archive/main/hukobu-air-main.zip) or select a specific one:

| Aircraft | Classic | Elite  | Elite Matte | Retro |
| --- |:---:| :---:| :---: | :---: |
| Airbus 320 | [Download](https://gitlab.com/draknova/hukobu-air/-/archive/main/hukobu-air-main.zip?path=Asobo_A320_HUAIR) | X | X | X |
| Airbus 320 (FlyByWire) | [Download](https://gitlab.com/draknova/hukobu-air/-/archive/main/hukobu-air-main.zip?path=Asobo_A320_FBW_HUAIR) | X | X | X |
| Boeing 747 | [Download](https://gitlab.com/draknova/hukobu-air/-/archive/main/hukobu-air-main.zip?path=Asobo_B747_HUAIR) | X | X | X |
| Boeing 787 | [Download](https://gitlab.com/draknova/hukobu-air/-/archive/main/hukobu-air-main.zip?path=Asobo_B787_HUAIR) | X | X | X |
| Cessna 172 (Classic) | [Download](https://gitlab.com/draknova/hukobu-air/-/archive/main/hukobu-air-main.zip?path=Asobo_C172_classic_HUAIR) | X | X | X |
| Cessna 172 (G1000) | [Download](https://gitlab.com/draknova/hukobu-air/-/archive/main/hukobu-air-main.zip?path=Asobo_C172_as1000_HUAIR) | X | X | X |
| Cessna 208B | [Download](https://gitlab.com/draknova/hukobu-air/-/archive/main/hukobu-air-main.zip?path=Asobo_208B_GRAND_CARAVAN_EX_HUAIR) | X | X | X |
| CJ4 | [Download](https://gitlab.com/draknova/hukobu-air/-/archive/main/hukobu-air-main.zip?path=Asobo_CJ4_HUAIR) | X | X | X |
| CRJ700 | [Download](https://gitlab.com/draknova/hukobu-air/-/archive/main/hukobu-air-main.zip?path=Asobo_CRJ700_HUAIR) | X | X | X |
| DA62 | [Download](https://gitlab.com/draknova/hukobu-air/-/archive/main/hukobu-air-main.zip?path=Asobo_DA62_HUAIR) | X | X | X |
| G36 | [Download](https://gitlab.com/draknova/hukobu-air/-/archive/main/hukobu-air-main.zip?path=Asobo_G36_HUAIR) | X | X | X |
| G58 | [Download](https://gitlab.com/draknova/hukobu-air/-/archive/main/hukobu-air-main.zip?path=Asobo_G58_HUAIR) | X | X | X |
| King Air 350i | [Download](https://gitlab.com/draknova/hukobu-air/-/archive/main/hukobu-air-main.zip?path=Asobo_KingAir350_HUAIR) | X | X | X |
| Longitude | [Download](https://gitlab.com/draknova/hukobu-air/-/archive/main/hukobu-air-main.zip?path=Asobo_Longitude_HUAIR) | [Download](https://gitlab.com/draknova/hukobu-air/-/archive/main/hukobu-air-main.zip?path=Asobo_Longitude_HUAIR_ELITE) | [Download](https://gitlab.com/draknova/hukobu-air/-/archive/main/hukobu-air-main.zip?path=Asobo_Longitude_HUAIR_ELITE_MATTE) | X |
| TBM 930 | [Download](https://gitlab.com/draknova/hukobu-air/-/archive/main/hukobu-air-main.zip?path=Asobo_TBM930_HUAIR) | [Download](https://gitlab.com/draknova/hukobu-air/-/archive/main/hukobu-air-main.zip?path=Asobo_TBM930_HUAIR_ELITE) | [Download](https://gitlab.com/draknova/hukobu-air/-/archive/main/hukobu-air-main.zip?path=Asobo_TBM930_HUAIR_ELITE_MATTE) | X |
| XCub | [Download](https://gitlab.com/draknova/hukobu-air/-/archive/main/hukobu-air-main.zip?path=Asobo_XCub_HUAIR) | X | X | X |


## Participating
Anyone can create or update the liveries. Just create a branch and submit a merge request on the Main.

## Livery Editor
I invite anyone who which to create liveries to use this tool: [MSFS Livery Editor](https://gitlab.com/draknova/msfs-livery-editor) 
